# yorick_disk_web

#### 介绍
一套以Kharna Admin为框架开发的个人网盘页面，配合yorick_disk项目可搭建完整的个人网盘网站

#### 软件架构
基于yorick_admin二次开发而来，使用BootStrap v4.1.3作为网页框架，jQuery v3.2.1作为脚本语言框架

#### 安装教程

1.  修改位于js/httpUtil.js 下baseUrl地址（后台服务地址）和imgUrl（用户头像图片http地址）
2.  打开login.html（登录界面）

#### 使用说明

1.  所有与后台交互的API逻辑代码放在主目录的js目录下
2.  js/httpUtil.js 作为网络请求脚本，用于与后台所有的API通讯，文件上传下载以及API健康检测（是否和后台断连）
3.  common.js 是一些公共函数的脚本
4.  核心文件夹及文件功能放在js/file.js下

#### 参与贡献

1.  Kharna Admin
2.  Yorick
3.  ChatGPT

#### 特技

1.  使用同步但不卡死页面线程的ajax处理方式，同步call api的同时不会卡死页面动画效果
2.  使用流式下载文件