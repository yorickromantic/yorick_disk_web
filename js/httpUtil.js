function httpPost(url,data){
    let res;
   $.when(getData(url,data)).done(function (json) {
       if(json.flag==-1){
           toastr.error("系统错误",json.msg);
           return
       }
       res = json;
   });
  return res;
}

function getData(url,data){
    var defer = $.Deferred();
    $.ajax({
        type: "POST",
        dataType:"json",
        url: url,
        contentType: 'application/json;charset=utf-8',
        data: JSON.stringify(data),
        async : false,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(json){
            defer.resolve(json);
        },
        error: function (xhr){
            if(xhr.status=='404'){
                location.href = "404.html"
            }
            toastr.error("连接服务器失败","网络异常");
        }
    });
    return defer;
}

window.onload = function(){
    clearInterval(interval);
    var interval = setInterval(function () {
        $.ajax({
            type: "GET",
            dataType:"json",
            url: baseUrl+"/common/apiHealth",
            contentType: 'application/json;charset=utf-8',
            data: {},
            async : true,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(json){
                if(json.flag==1){
                   console.log("-----------heartbeat-----------")
                }else{
                    toastr.error("连接服务器失败","网络异常");
                    clearInterval(interval)
                }
            },
            error: function (xhr){
                toastr.error("连接服务器失败","网络异常");
                clearInterval(interval);
            }
        });
    }, 5 * 1000);
}


function uploadFile(apiUrl,data,processName,callback){
    var xhr = new XMLHttpRequest();
    xhr.upload.onprogress = function (evt) {
        //侦查附件上传情况
        //通过事件对象侦查
        //该匿名函数表达式大概0.05-0.1秒执行一次
        //console.log(evt);
        //console.log(evt.loaded);  //已经上传大小情况
        //evt.total; 附件总大小
        var loaded = evt.loaded;
        var tot = evt.total;
        var per = Math.floor(100 * loaded / tot);  //已经上传的百分比
        $(processName).html("<div class=\"progress\">" +
            "                  <div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\" aria-valuenow='"+per+"' aria-valuemin=\"0\" aria-valuemax=\"100\" style='width: "+per+"%'>" +
            "                  <span class=\"sr-only\">"+per+"% Complete</span>" +
            "                  </div>" +
            "                  </div>");
    }
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            callback(xhr.responseText)
        }
    }
    xhr.open("post", apiUrl,true);
    xhr.withCredentials = true;
    xhr.crossDomain = true;
    xhr.send(data);
}

function fetchFile(apiUrl, data, callback){
    fetch(apiUrl,{
        credentials: 'include',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    },)
        .then(response => response.blob())
        .then(blob => {
            callback(blob)
        })
        .catch(error => {
            console.error('Error downloading file:', error);
        });
}

var baseUrl="http://localhost:9090/yorick_disk"
var imgUrl = "http://localhost:8080/resources/"