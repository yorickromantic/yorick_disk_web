document.write("<script src='js/httpUtil.js'></script>");
document.write("<script src='js/common.js'></script>");

$(document).ready(function () {
    $('.counter').countUp();
    $(".banner-name").html(ls.getItem("username"))
    $(".banner-account").html(ls.getItem("account"))
    $(".banner-head").attr("src", imgUrl+ls.getItem("head"));
})

$("#changePasswordPanelBtn").click(function () {
    $("#changePasswordModal").modal('show')
})

$("#changePasswordBtn").click(function () {
    var oldPassword = $("#oldPassword").val();
    var newPassword = $("#newPassword").val();
    var newPassword2 = $("#newPassword2").val();
    yorickAssert(oldPassword != "" && newPassword != ""
        && newPassword2, "请输入完整参数", "参数错误")
    yorickAssert(newPassword == newPassword2, "两次密码不一致", "新密码不一致")
    var jsonObj =
        {
            "password":newPassword2,
            "oldPassword":oldPassword
        }
    var result=httpPost(baseUrl+"/account/changePassword",jsonObj);
    if(result.flag==1) {
        toastr.success("修改密码成功", "成功");
        $('#changePasswordModal').modal('hide');
    }else{
        toastr.error("系统错误", result.msg);
    }
})

$("#logoutBtn").click(function () {
    var result=httpPost(baseUrl+"/login/doLogout","");
    if(result.flag==1){
        ls.clear()
        location.href="login.html"
    }else{
        toastr.error("请联系管理员",result.msg);
    }
})