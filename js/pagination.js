function pagination(totalCount, pageSize, currentPage, callback) {

    var pages = Math.ceil(totalCount / pageSize);
    var beforeBtn = "";
    var afterBtn = "";
    var prevDisabled = "disabled";
    var nextDisabled = "disabled";
    if (currentPage > 1) {
        prevDisabled = "";
        var beforeAmount = parseInt(currentPage) - parseInt(1)
        beforeBtn =
            "<li class=\"page-item\">" +
            "<span class=\"page-link\" id='beforePageBtn'>" + beforeAmount + "</span>" +
            "</li>"
    }
    if (currentPage < pages) {
        nextDisabled = "";
        var afterAmount = parseInt(currentPage) + parseInt(1)
        afterBtn =
            "<li class=\"page-item\">" +
            "<span class=\"page-link\" id='afterPageBtn'>" + afterAmount + "</span>" +
            "</li>"
    }

    var pagination = "<nav>\n" +
        "<ul class=\"pagination mb-0\">" +
        "<li class=\"page-item " + prevDisabled + "\">" +
        "<span class=\"page-link\" id='prevPageBtn'>Prev</span>" +
        "</li>" +
        beforeBtn +
        "<li class=\"page-item active\">" +
        "<span class=\"page-link\">" +
        currentPage +
        "<span class=\"sr-only\">(current)</span>" +
        "</span>" +
        "</li>" +
        afterBtn +
        "<li class=\"page-item " + nextDisabled + "\">" +
        "<span class=\"page-link\" id='nextPageBtn'>Next</span>" +
        "</li>" +
        "</ul>" +
        "</nav>"

    $("#pagination").html(pagination);

    $("#beforePageBtn").click(function () {
        callback("prev", currentPage)
    })
    $("#afterPageBtn").click(function () {
        callback("next", currentPage)
    })
    $("#prevPageBtn").click(function () {
        callback("prev", currentPage)
    })
    $("#nextPageBtn").click(function () {
        callback("next", currentPage)
    })
}